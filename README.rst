Memilah Data Produk Lazada
==========================
Aplikasi ini berguna untuk memilah produk yang ada di Lazada_. Tujuan
akhirnya memudahkan pencarian produk seperti: "Dapatkan harga notebook maksimal
Rp 5.000.000".

Dibutuhkan scrapy untuk menjalankannya. Sudah teruji di Scrapy 1.0.3.

Pemasangan
----------
Buatlah Python virtual environment::

  virtualenv env

Lalu pasanglah scrapy::

  env/bin/pip install -r lazada/requirements.txt

Tidak ada pemasangan driver database di sini.

Pengambilan Data
----------------
Data yang diambil akan disimpan dalam sebuah file bertipe JSON.
Untuk mengambil daftar produk notebook::

  cd lazada 
  ../env/bin/scrapy crawl notebook -t json \
      -o /home/webdate/tmp/lazada/notebook.json
  
Jika Anda ingin menyimpan file HTML setiap produk di sebuah direktori gunakan
`Spider Arguments`_ ``-a save_dir=/path/target``. Tapi sebelumnya
hapuslah file ``notebook.json`` agar tidak membengkak::

  rm -f notebook.json
  mkdir -p /home/webdate/tmp/lazada/notebook  
  ../env/bin/scrapy crawl notebook -t json \
      -o /home/webdate/tmp/lazada/notebook.json \
      -a save_dir=/home/webdate/tmp/lazada/notebook
  
Merasa ada penerjemahan yang kurang pas ? Setelah Anda memperbaiki source maka
file HTML tadi bisa dibaca kembali, jadi tidak perlu unduh ulang::

  ../env/bin/scrapy crawl notebook -t json \
      -o /home/webdate/tmp/lazada/notebook.json \
      -a product_url=file:///home/webdate/tmp/lazada/notebook

Apakah pengambilan data produk tertentu ada yang kurang pas ? Perbaikilah source-nya.
Setelah itu Anda bisa unduh halaman produk itu saja agar lebih cepat, sekedar memastikan
apakah perbaikan sudah sesuai::

  ../env/bin/scrapy crawl notebook -t json \
      -a product_url=http://www.lazada.co.id/asus-x455la-wx080d-2-gb-ram-intel-ci3-4030u-14-biru-922255.html \
      -a save_dir=/home/webdate/tmp/lazada/notebook

Jika sudah pas jalankan kembali perintah sebelumnya agar seluruh produk diterjemahkan ulang::

  rm -f notebook.json
  ../env/bin/scrapy crawl notebook -t json \
      -o /home/webdate/tmp/lazada/notebook.json \
      -a product_url=file:///home/webdate/tmp/lazada/notebook

Biasanya sebuah toko online akan memperbaharui datanya setiap hari. Oleh karena
itu kita perlu meletakkan perintah ini pada sistem *cron*. Untuk pemantauan
aktivitasnya diperlukan pencatatan ke *log file*::

  cd /home/webdate
  mkdir -p tmp/lazada/notebook
  rm -f tmp/lazada/notebook.json
  cd lazada
  ../env/bin/scrapy crawl notebook -t json \
      -o /home/webdate/tmp/lazada/notebook.json \
      -a save_dir=/home/webdate/tmp/lazada/notebook \
      --logfile=/home/webdate/tmp/lazada/notebook.log

Selanjutnya kita bisa akhiri script di atas dengan script penyalin ke database
bersumber dari ``notebook.json``, tidak disertakan di sini.

Kategori lainnya selain ``notebook`` adalah ``desktop`` dan ``mobile_phone``.
Selamat mencoba.

.. _Lazada: http://lazada.co.id
.. _Spider Arguments: http://doc.scrapy.org/en/latest/topics/spiders.html#spider-arguments
