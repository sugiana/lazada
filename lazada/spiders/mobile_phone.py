import os
import re
from ..items import MobilePhone
from .notebook import NotebookSpider
from .tools import (
    should_int,
    regex_compile,
    regex_units_compile,
    c_regex_search,
    clean,
    REGEX_UNITS,
    one_space,
    )


def regex_get_original_words(pattern, str_lower, str_orig):
    pattern = '({s})'.format(s=pattern)
    match = re.compile(pattern).search(str_lower)
    found = match.group(1)
    p = str_lower.find(found)
    return str_orig[p:p+len(found)]

##########
# Memory #
##########
MEMORY_REGEXS = [
    '^(\d*)$', # 2 
    '(\d*) %s' % REGEX_UNITS, # 2 GB
    '(\d*)%s' % REGEX_UNITS, # 4GB
    ]
MEMORY_REGEXS = regex_compile(MEMORY_REGEXS)

def parse_memory(values):
    for value in values:
        s = value.replace('\n', ' ')
        s = clean(s).lower()
        try:
            amount = int(s)
            if amount > 99:
                unit = 'mb'
            else:
                unit = 'gb'
            value = '{a} {u}'.format(a=amount, u=unit.upper())
            return [value, amount, unit]
        except ValueError:
            pass
        for rc in MEMORY_REGEXS:
            match = c_regex_search(rc, s)
            if match:
                amount = match.group(1)
                unit = match.group(2)
                return [value, int(amount), unit]

MEMORY_REGEXS_FROM_DESCRIPTION = regex_units_compile([
    'ram : (\d*) %s', # RAM : 512 MB
    'ram (\d*)%s', # RAM 4GB
    'ram (\d*) %s', # RAM 1 GB
    'ram (\d\.\d)%s', # RAM 1.5GB
    'ram (\d\.\d) %s', # RAM 1.5 GB
    'ram berkapasitas (\d*) %s', # RAM berkapasitas 1 GB
    'ram sebesar (\d*) %s', # RAM sebesar 1 GB
    '(\d*) %s ram', # 768 MB RAM    
    '(\d*)%s ram', # 3GB RAM
    ])

MEMORY_REGEXS_TYPO = (
    (re.compile('ram (\d*)gg'), 'gb'),
    )

def parse_memory_from_description(values):
    for value in values:
        val = value.replace('\n', ' ')
        val = clean(val)
        s = val.lower()
        for rc in MEMORY_REGEXS_FROM_DESCRIPTION:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = should_int(float(match.group(1)))
            unit = match.group(2)
            value = regex_get_original_words(rc.pattern, s, val)
            return [value, amount, unit]
    for value in values:
        val = value.replace('\n', ' ')
        val = clean(val)
        s = val.lower()
        for rc, unit in MEMORY_REGEXS_TYPO:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = should_int(float(match.group(1)))
            value = regex_get_original_words(rc.pattern, s, val)
            return [value, amount, unit]


MEMORY_TYPO = [
    ((512, 'gb'), (512, 'mb')),
    ]
    
def memory_anti_typo(vals):
    current_amount = vals[1]
    current_unit = vals[2]
    for before, after in MEMORY_TYPO:
        before_amount, before_unit = before
        after_amount, after_unit = after
        if before_amount == current_amount and before_unit == current_unit:
            return [vals[0], after_amount, after_unit]
    return vals

###########
# Storage #
###########
STORAGE_REGEXS = [
    '(\d*\.\d)', # 8.0
    '^(\d*)$', # 8    
    '(\d*) %s' % REGEX_UNITS, # 8 GB
    '(\d*)%s' % REGEX_UNITS, # 8GB
    ]
STORAGE_REGEXS = regex_compile(STORAGE_REGEXS)

def parse_storage(values):
    for value in values:
        s = clean(value).lower()
        for rc in STORAGE_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = should_int(float(match.group(1)))
            if len(match.groups()) == 1:
                unit = 'gb'
                value = '{n} {u}'.format(n=int(amount), u=unit.upper())
            else:
                unit = match.group(2)
            return [value, amount, unit]

STORAGE_REGEXS_FROM_DESCRIPTION = regex_units_compile([
    '(\d*)%s micro sd', # 8GB Micro SD
    'internal (\d*)%s', # Internal 8GB
    'internal (\d*) %s', # internal 16 GB
    'rom (\d*)%s', # ROM 32GB
    'rom (\d*) %s', # ROM 32 GB
    '^(\d\d)%s rom', # 32GB ROM
    '/(\d\d)%s', # /64GB
    ])

def parse_storage_from_description(values):
    for value in values:
        val = clean(value)
        s = val.lower()
        for rc in STORAGE_REGEXS_FROM_DESCRIPTION:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = should_int(float(match.group(1)))
            unit = match.group(2)
            value = regex_get_original_words(rc.pattern, s, val)
            return [value, amount, unit]

BATTERY_REGEXS = [
    '(\d*) mah', # 1540 mAh
    '(\d*)mah', # 2540mAh
    ]
BATTERY_REGEXS = regex_compile(BATTERY_REGEXS)
    
def parse_battery(values):
    for value in values:
        s = clean(value).lower()
        for rc in BATTERY_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = match.group(1)
            if amount:
                return [value, int(amount)]

##############
# Resolution #
##############
RESOLUTION_REGEXS = [
    '(\d*) x (\d*)', # 940 x 480
    '(\d*)x(\d*)', # 1920x1080
    ]
RESOLUTION_REGEXS = regex_compile(RESOLUTION_REGEXS)

def parse_resolution(values):
    for value in values:
        s = clean(value).lower()
        for rc in RESOLUTION_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            count = len(match.groups())
            if not count:
                continue
            if not match.group(1):
                continue
            if count > 1:
                if not match.group(2):
                    continue
                width = int(match.group(1).replace('.', ''))
                height = int(match.group(2).replace('.', ''))
            else:
                width = None
                height = int(match.group(1).replace('.', ''))
            if width < 99 or height < 99:
                continue
            return [value, width, height]

# Monitor
DEFAULT_MONITOR = {
    'samsung galaxy s8': 5.8,
    }

class MobilePhoneSpider(NotebookSpider):
    name = 'mobile_phone'
    start_urls = ('http://www.lazada.co.id/beli-smartphone/',)
    specs = dict(
        processor=('Tipe Processor',),
        memory=('RAM', 'RAM memory'),
        storage=('Kapasitas Penyimpanan', 'Kapasitas Harddisk'),
        monitor=('Ukuran layar', 'Screen Size (inches)', 'Size'),
        resolution=('Resolusi Layar', 'Resolution'),
        operating_system=('Versi Sistem Operasi', 'Sistem Operasi'),
        rear_camera=('Kamera Belakang (Megapiksel)',),
        front_camera=('Kamera Depan (Megapiksel)',),
        battery=('Kapasitas Baterai',),
        model=('Model',),
        story=('Product description',),
        )
    product_class = MobilePhone
    blacklist_brand_file = os.path.join('blacklist', 'brand',
        'mobile_phone.txt')
    
    # Override parent method    
    def parse_storage(self, response, i):
        if 'storage' in i:
            s = i['storage']
            r = parse_storage(s)
            if r:
                i['storage'] = r
                return i
            vals = s
        else:
            vals = []
        s = i['description']
        v = parse_storage_from_description(s)
        vals += s
        if v:
            i['storage'] = v
        else:
            s = [i['title']]
            v = parse_storage_from_description(s)
            vals += s
            if v:
                i['storage'] = v
            else:
                self.log_tidak_paham(response, vals, 'storage')
                if 'storage' in i:
                    del i['storage']
        return i
        
    # Override parent method
    def parse_before_plain_description(self, response, i):
        i = self.parse_resolution(response, i)
        i = self.parse_battery(response, i)
        i = self.parse_rear_camera(response, i)
        i = self.parse_front_camera(response, i)
        return i
        
    # override parent method
    def parse_memory(self, response, i):        
        v = None
        vals = []
        if 'memory' in i:
            vals = i['memory']
            v = parse_memory(vals)
        if not v:
            s = i['description']
            v = parse_memory_from_description(s)
            vals += s
        if not v:
            s = [i['title']]
            v = parse_memory_from_description(s)
            vals += s
        if not v and 'story' in i:
            for s in i['story']:
                s = [s]
                v = parse_memory_from_description(s)
                if v:
                    break
                vals += s
        if v:
            i['memory'] = memory_anti_typo(v)
        else:
            self.log_tidak_paham(response, vals, 'memory')
        return i
        
    # override
    def parse_monitor(self, response, i):
        title = one_space(i['title']).lower()
        for key in DEFAULT_MONITOR:
            if title.find(key) > -1:
                val = DEFAULT_MONITOR[key]
                label = '%s"' % val
                i['monitor'] = [label, val] 
                return i
        return NotebookSpider.parse_monitor(self, response, i)

    def parse_battery(self, response, i):
        if 'battery' not in i:
            return i
        val = i['battery'][0]
        try:
            amount = int(val)
            s = '{n} mAh'.format(n=amount)
            i['battery'] = [s, amount]
            return i
        except ValueError:
            pass
        capacity = parse_battery(i['battery'])
        if capacity:
            i['battery'] = capacity
        else:
            del i['battery']
        return i
        
    def parse_rear_camera(self, response, i):
        if 'rear_camera' not in i:
            return i
        val = i['rear_camera'][0]
        try:
            amount = float(val)
            amount = should_int(amount)
            s = '{n} MP'.format(n=amount)
            i['rear_camera'] = [s, amount]
        except ValueError:
            self.log_tidak_paham(response, val, 'rear_camera')
        return i
        
    def parse_front_camera(self, response, i):
        if 'front_camera' not in i:
            return i
        val = i['front_camera'][0]
        try:
            amount = float(val)
            amount = should_int(amount)
            s = '{n} MP'.format(n=amount)
            i['front_camera'] = [s, amount]
        except ValueError:
            self.log_tidak_paham(response, val, 'front_camera')
        return i
        
    # Override parent method
    def parse_resolution(self, response, i):
        if 'resolution' not in i:
            return super(MobilePhoneSpider, self).parse_resolution(response, i)
        res = parse_resolution(i['resolution'])
        if res:
            i['resolution'] = res
        else:
            del i['resolution']
        return i
