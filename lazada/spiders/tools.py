import os
import re
import json
import string
from glob import glob
from urlparse import urlsplit
from bs4 import BeautifulSoup
from scrapy import Request
from scrapy.selector import Selector
from scrapy.spiders import Spider
from scrapy.exceptions import CloseSpider


BASE_URL = 'http://www.lazada.co.id'
XPATH_DESCRIPTIONS = (
    '//div[@class="product-description__block"]/p/text()',
    '//div[@class="inbox__wrap"]/ul/li/span/text()',
    '//div[@class="prod_details "]/ul/li/span/text()',
    )
XPATH_SPECS = (
    '//table[@class="specification-table"]/tr/td/text()',
    '//div[@class="product-description__block"]/table/tbody/tr/td/text()',
    )
XPATH_IMAGE = '//div[@id="productZoom"]/@data-zoom-image'
REGEX_DATA = re.compile('dataLayer.push\((.*)\);')

def get_description(sel):
    r = []
    for s_xpath in XPATH_DESCRIPTIONS:
        for s in sel.xpath(s_xpath):
            v = s.extract()
            v = clean(v)
            if v:
                r.append(v)
    return r
 
def get_image(sel):
    for s in sel.xpath(XPATH_IMAGE):
        return s.extract()

def get_specs_(sel, tpl):
    r = dict()
    xs_spec = sel.xpath(tpl)
    key = None
    for s in xs_spec:
        val = s.extract()
        val = clean(val)
        if key is None:
            key = val
            continue
        r[key] = [val]
        key = None
    return r

def get_specs(sel):
    r = dict()
    for t in XPATH_SPECS:
        r = get_specs_(sel, t)
        if r:
            break
    return r
                
def file_as_list(filename):
    if not os.path.exists(filename):
        return []
    s_list = []
    f = open(filename)
    for line in f.readlines():
        s = line.strip()
        s_list.append(s)
    f.close()
    return s_list

def is_url_local(url):
    return url.find('file://') > -1
    
def url_from_response(response):
    if is_url_local(response.url):
        return 'http://www.lazada.co.id/' + os.path.split(response.url)[-1]
    return response.url

def parse_price(s):
    return [s, should_int(float(s)), 'idr']
    
def get_start_urls(url):
    if not is_url_local(url):
        return [url]
    filename = url.replace('file://','').rstrip('/')
    if os.path.isfile(filename):
        return [url]
    pattern = filename + '/*.html'
    r = []
    for filename in glob(pattern):
        url_local = 'file://' + filename
        r.append(url_local)
    return r
    
def is_html_suffix(s):
    t = s.split('?')[0]
    return t[-5:] == '.html'
        

class CommonSpider(Spider):
    allowed_domains = ('lazada.co.id',)
    start_page = None
    product_class = None # override please
    blacklist_brand_file = None
    
    def __init__(self, product_url=None, save_dir=None, *args, **kwargs):
        super(CommonSpider, self).__init__(*args, **kwargs)
        self.product_url = product_url
        self.save_dir = save_dir
        if product_url:
            self.start_urls = get_start_urls(product_url)
        self.blacklist_brand = self.blacklist_brand_file and \
            file_as_list(self.blacklist_brand_file) or []

    # Override parent method
    def parse(self, response):
        if self.product_url:
            if is_html_suffix(response.url):
                yield self.parse_product(response)
            return
        xs = Selector(response)
        for xs_json in xs.xpath('//div[@class="c-product-card__gallery-img-wrapper"]').xpath('@data-group'):
            s_json = xs_json.extract()
            d = json.loads(s_json)
            url_ = BASE_URL + d['url']
            if is_html_suffix(url_):
                yield Request(url=url_, callback=self.parse_product_)
        xs_next = xs.xpath('//div[@class="page-link page-link--next"]')
        if xs_next:
            selector = Selector(text=xs_next.extract()[0])
            urls = selector.xpath('//a').xpath('@href').extract()
            if urls:
                url = BASE_URL + urls[0]
                yield Request(url=url)

    def product_instance(self, response):
        return self.product_class()
        
    def parse_product_(self, response):
        if is_html_suffix(response.url):
            yield self.parse_product(response)

    def parse_product(self, response):
        if self.save_dir:
            self.save(response)
        match = REGEX_DATA.search(response.body)
        json_text = match.group(1)
        data = json.loads(json_text)
        if self.is_blacklist(response, data):
            return
        i = self.product_instance(response)
        i['url'] = url_from_response(response)            
        i['brand'] = data['brand'].strip().lower()
        product = data['page']['product']
        i['title'] = product['name'].replace('\n', ' ')
        i['price'] = parse_price(product['price'])
        i['stock'] = product['inStock'] and 1 or 0
        sel = Selector(response)
        i['description'] = get_description(sel)
        picture = get_image(sel)
        if picture:
            i['picture'] = picture
        specs = get_specs(sel)
        for key in specs:
            vals = specs[key]
            if not vals:
                continue
            for spec_name in self.specs:
                spec_vals = self.specs[spec_name]
                if key in spec_vals:
                    i[spec_name] = vals
                    break
        return i

    def save(self, response):
        u = urlsplit(response.url)
        filename = u.path.lstrip('/')
        if not filename:
            return
        fullpath = os.path.join(self.save_dir, filename)
        f = open(fullpath, 'w')
        f.write(response.body)
        f.close()
        
    def is_blacklist(self, response, data):
        if 'brand' in data:
            b = data['brand'].strip().lower()
            if b in self.blacklist_brand:
                self.logger.info('{url} brand {b} di-blacklist.'.\
                    format(url=response.url, b=b))
                return True

    def log_tidak_paham(self, response, vals, name):
        self.logger.warning('{url} {vals} {n} tidak dipahami.'.format(
            url=response.url, vals=vals, n=name))
 
     
######################
# Regular Expression #
######################
def regex_compile(regexs):
    r = []
    for regex in regexs:
        c = re.compile(regex)
        r.append(c)
    return r

def regex_units_compile(templates):
    r = []
    for tpl in templates:
        jml = tpl.count('%s')
        ru = tuple()
        for i in range(jml):
            ru += (REGEX_UNITS,)
        s = tpl % ru 
        c = re.compile(s)
        r.append(c)
    return r
   
def c_regex_search(c_regex, text):
    return c_regex.search(text.lower())
    
def regex_get_original_words(pattern, str_lower, str_orig):
    pattern = '({s})'.format(s=pattern)
    match = re.compile(pattern).search(str_lower)
    found = match.group(1)
    p = str_lower.find(found)
    return str_orig[p:p+len(found)]
        
##########
# String #
##########
def just_ascii(s):
    r = ''
    for ch in s:
        if ord(ch) > 127:
            ch = ' '
        r += ch
    return r

def clean_char(ch):
    ch = ch.replace('\u201d', '"')
    ch = ch.replace('\r', ' ')
    ch = ch.replace(u'\xa0', ' ')
    ch = ch.replace(u'\xd7', 'x')
    return ch in string.printable and ch or ' '

def one_space(s):
    while s.find('  ') > -1:
        s = s.replace('  ', ' ')
    return s
    
def clean(s):
    s = ''.join([clean_char(ch) for ch in s])
    s = s.replace('\t', ' ')
    s = one_space(s)
    s = s.strip()
    s = s.replace('&quot;', '"')    
    if s.find('<span') < 0 and s.find('<br') < 0:
        return s
    soup = BeautifulSoup(s, 'lxml')
    for link in soup.find_all('span'):
        link.unwrap()
    return str(soup).replace('<br/>', '\n').\
            lstrip('<html><body><p>').\
            rstrip('</p></body></html>')
    
def clean_list(vals):
    r = []
    for s in vals:
        val = clean_(s)
        r.append(val)
    return r

###########
# Numeric #
###########    
def should_int(value):
    int_ = int(value)
    return int_ == value and int_ or value

MBs = dict(
        mb=1,
        gb=1024,
        tb=1024*1024,
        )
        
##########
# Memory #
##########
UNITS = MBs.keys()
REGEX_UNITS = '(%s)' % '|'.join(UNITS)
