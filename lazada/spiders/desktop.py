import os
import re
from ..items import Desktop
from .tools import (
    CommonSpider,
    should_int,
    regex_compile,
    regex_units_compile,
    c_regex_search,
    clean,
    REGEX_UNITS,
    regex_get_original_words,
    MBs,
    )


#############
# Processor #
#############
PROCESSOR_REGEXS = regex_compile([
    'intel(.*)celeron',
    'intel core',
    '(\d*)ghz',
    'intel(.*)28(\.d)0',
    'dual core',
    'dualcore',
    'quad core',
    'quad-core',
    'quadcore',
    'core i3',
    'processor i3',
    'core(.*)i5',
    'i5(.*)processor',
    ' i5 ',
    'i7-',
    ' i7 ',
    'processor intel',
    'atom',
    'pentium',
    'proc: amd',
    'proc amd',
    'amd a8',
    'amd e2',
    ' a4-',
    ])

def parse_processor(items):
    for value in items['description']:
        s = value.replace('\n', ' ')
        s = clean(s).lower()
        for rc in PROCESSOR_REGEXS:
            match = c_regex_search(rc, s)
            if match:
                return value

##########
# Memory #
##########
MEMORY_REGEXS = regex_compile([
    '^(\d*)$', # 2 
    '(\d*) %s' % REGEX_UNITS, # 2 GB
    '(\d*)%s' % REGEX_UNITS, # 4GB
    ])

def parse_memory(values):
    for value in values:
        s = value.replace('\n', ' ')
        s = clean(s).lower()
        try:
            amount = int(s)
            if amount > 99:
                unit = 'mb'
            else:
                unit = 'gb'
            value = '{a} {u}'.format(a=amount, u=unit.upper())
            return [value, amount, unit]
        except ValueError:
            pass
        for rc in MEMORY_REGEXS:
            match = c_regex_search(rc, s)
            if match:
                amount = match.group(1)
                unit = match.group(2)
                return [value, int(amount), unit]

MEMORY_REGEXS_FROM_DESCRIPTION = regex_units_compile([
    'ram (\d*)%s', # RAM 4GB
    'memory.(\d*)%s', # memory\n8GB 
    'ram (\d*) %s', # RAM 1 GB
    'ram: (\d*)%s', # RAM: 64GB
    'ram (\d\.\d)%s', # RAM 1.5GB
    'ram berkapasitas (\d*) %s', # RAM berkapasitas 1 GB
    'ddr3 (\d*) %s', # DDR3 2 GBM
    '(\d*) %s ddr', # 2 GB DDR3
    '(\d*) %s ram', # 768 MB RAM    
    '(\d*)%s ram', # 3GB RAM
    '(\d*)%s ddr', # 2GB DDR3
    ])

MAX_MEMORY = 64 * MBs['gb']

def parse_memory_from_description(values):
    for value in values:
        val = value.replace('\n', ' ')
        val = clean(val)
        s = val.lower()
        for rc in MEMORY_REGEXS_FROM_DESCRIPTION:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = match.group(1)
            if not amount:
                continue
            amount = should_int(float(amount))
            unit = match.group(2)
            MBytes = amount * MBs[unit]
            if MBytes > MAX_MEMORY:
                continue
            value = regex_get_original_words(rc.pattern, s, val)
            return [value, amount, unit]

###########
# Storage #
###########
STORAGE_REGEXS = regex_compile([
    '(\d*\.\d)', # 8.0
    '^(\d*)$', # 8    
    '(\d*) %s' % REGEX_UNITS, # 8 GB
    '(\d*)%s' % REGEX_UNITS, # 8GB
    ])

def parse_storage(values):
    for value in values:
        s = clean(value).lower()
        for rc in STORAGE_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = float(match.group(1))
            if not amount:
                continue
            amount = should_int(amount)
            if len(match.groups()) == 1:
                unit = 'gb'
                value = '{n} {u}'.format(n=int(amount), u=unit.upper())
            else:
                unit = match.group(2)
            return [value, amount, unit]

STORAGE_REGEXS_FROM_DESCRIPTION = regex_units_compile([
    '(\d*)%s micro sd', # 8GB Micro SD
    'internal (\d*)%s', # Internal 8GB
    '(\d\d\d) (%s)', # 500 GB,
    '(\d\d\d)(%s)', # 500 GB,
    'kapasitas penyimpanan (\d*)%s', # 1TB Hard Drive
    'hardisk (\d*)%s', # hardisk 1TB
    '(\d*)%s dvd', # 160gb Dvd
    'sata \((\d*)%s\)', # SATA (1TB)
    '(\d*)%s hard drive', # 1TB hard drive
    ])

def parse_storage_from_description(values):
    for value in values:
        val = clean(value)
        s = val.lower()
        for rc in STORAGE_REGEXS_FROM_DESCRIPTION:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = float(match.group(1))
            if not amount:
                continue
            amount = should_int(amount)
            unit = match.group(2)
            value = regex_get_original_words(rc.pattern, s, val)
            return [value, amount, unit]

###########
# Monitor #
###########
MONITOR_REGEXS = regex_compile([
    '(\d*\.\d) inch', # 12.5 inch
    '(\d*\.\d)"', # 12.5"
    '(\d*\.\d)" wxga', # 10.1" wxga
    '(\d*\,\d)" led', # 11,6" led    
    '(\d*\.\d) wxga', # 10.1 wxga
    '(\d*)" wxga', # 12" wxga
    '(\d*) inch', # 12 inch
    '(\d*)"', # 12"
    '(\d*\.\d)', # 20.0 
    '(\d*)', # 20
    ])

def parse_monitor(values):
    for value in values:
        val = clean(value)
        s = val.lower()
        for rc in MONITOR_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = match.group(1)
            if not amount:
                continue
            try:
                amount = float(s)
            except ValueError:
                continue
            if amount:
                amount = should_int(float(s))
                value = '{n}"'.format(n=amount)                
                return [value, amount]

MONITOR_REGEXS_FROM_DESCRIPTION = regex_compile([
    '(\d*,\d)" led', # 11,6" led
    "(\d*\.\d)'led", # 10.1'LED
    '(\d*) inci', # 5 inci
    '(\d*) inch', # 2 inch
    '(\d*). inch', # 5. Inch
    '(\d\.\d) inch', # 5.5 inch
    '(\d\d)"', # 14"
    'hd (\d)"', # HD 5"
    ])
    
def parse_monitor_from_description(values):
    for value in values:
        val = clean(value)
        s = val.lower()
        for rc in MONITOR_REGEXS_FROM_DESCRIPTION:
            match = c_regex_search(rc, s)
            if not match:
                continue
            amount = match.group(1).replace(',', '.')
            if not amount:
                continue
            try:
                amount = float(amount)
            except ValueError:
                continue
            if amount:
                amount = should_int(amount)
                value = regex_get_original_words(rc.pattern, s, val)                
                return [value, amount]


UNIT_PRICE = 3300000
MAX_MEMORY_WITH_UNIT_PRICE = 64 * MBs['gb']

class DesktopSpider(CommonSpider):
    name = 'desktop'
    start_urls = ('http://www.lazada.co.id/beli-pc-all-in-one/',)
    specs = dict(
        processor=('Tipe Processor', 'Processor Type'),
        memory=('RAM', 'Memori RAM (Gb)'),
        storage=('Ukuran hard disk (GB)', 'Hard Drive Capacity'),
        monitor=('Ukuran layar',),
        operating_system=('Versi Sistem Operasi', 'Sistem Operasi'),
        model=('Model',),
        story=('Product description',),
        )
    product_class = Desktop
    blacklist_brand_file = os.path.join('blacklist', 'brand', 'desktop.txt')
    
    # Override parent method
    def parse_product(self, response):
        i = super(DesktopSpider, self).parse_product(response)
        if not i:
            return
        i = self.parse_title(response, i)
        i = self.parse_processor(response, i)
        i = self.parse_memory(response, i)
        i = self.parse_storage(response, i)
        i = self.parse_monitor(response, i)
        i = self.parse_model(response, i)
        i = self.parse_operating_system(response, i)
        i = self.parse_before_plain_description(response, i)
        i = self.parse_description(response, i)
        return i    
        
    def parse_title(self, response, i):        
        i['title'] = clean(i['title'])
        return i
        
    def parse_model(self, response, i):
        if 'model' in i:
            i['model'] = i['model'][0]
        return i        

    def parse_processor(self, response, i):        
        if 'processor' not in i:
            return i
        processor = parse_processor(i)
        if processor:
            i['processor'] = processor
        else:
            i['processor'] = i['processor'][0]
            #vals = i['description']
            #if vals:
            #    self.logger.warning('{url} {vals} processor tidak '\
            #        'dipahami.'.format(url=response.url, vals=vals))
        return i
            
    def parse_storage(self, response, i):
        if 'storage' in i:
            s = i['storage']
            r = parse_storage(s)
            if r:
                i['storage'] = r
                return i
            vals = s
        else:
            vals = []
        s = i['description'] + [i['title']]
        v = parse_storage_from_description(s)
        vals += s
        if v:
            i['storage'] = v
        else:
            self.log_tidak_paham(response, vals, 'storage')
            if 'storage' in i:
                del i['storage']
        return i
                
    def parse_memory(self, response, i):        
        v = None
        vals = []
        if 'memory' in i:
            vals = i['memory']
            v = parse_memory(vals)
        if not v:
            s = i['description']
            v = parse_memory_from_description(s)
            vals += s
        if not v:
            s = [i['title']]
            v = parse_memory_from_description(s)
            vals += s
        if not v and 'story' in i:
            for s in i['story']:
                s = [s]
                v = parse_memory_from_description(s)
                if v:
                    break
                vals += s
        if v:
            amount, unit = v[1:]
            MBytes = amount * MBs[unit]
            price = i['price'][1]
            if MBytes >= MAX_MEMORY_WITH_UNIT_PRICE and price < UNIT_PRICE:
                self.logger.warning('{url} memory {amount} {unit} tidak '\
                    'mungkin unit-nya seharga Rp {price}'.format(
                    url=response.url,
                    amount=amount, unit=unit.upper(), price=price))
                v = None
        if v:
            i['memory'] = v        
        else:
            self.log_tidak_paham(response, vals, 'memory')
            if 'memory' in i:
                del i['memory']
        return i
        
    def parse_operating_system(self, response, i):        
        if 'operating_system' in i:
            i['operating_system'] = i['operating_system'][0]
        return i        
        
    def parse_before_plain_description(self, response, i):
        return i
        
    def parse_monitor(self, response, i):
        if 'monitor' in i:
            s = i['monitor']
            r = parse_monitor(s)
            if r:
                i['monitor'] = r
                return i
            vals = s
        else:
            vals = []
        s = i['description']
        v = parse_monitor_from_description(s)
        if v:
            i['monitor'] = v
        else:
            vals += s
            s = [i['title']]
            v = parse_monitor_from_description(s)
            if v:
                i['monitor'] = v
            else:
                self.log_tidak_paham(response, vals, 'monitor')
                if 'monitor' in i:
                    del i['monitor']
        return i
        
    def parse_description(self, response, i):        
        i['description'] = ', '.join(i['description'])
        return i        
