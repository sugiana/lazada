import os
from ..items import Notebook
from .desktop import DesktopSpider
from .tools import (
    clean,
    REGEX_UNITS,
    regex_compile,
    c_regex_search,
    )


##############
# Resolution #
##############
RESOLUTION_REGEXS = [
    '(\d\.\d*) x (\d*)', # 1.280 x 720 piksel
    '(\d*)-by-(\d*)', # 2304-by-1440 resolution at 226 pixels per inch with support for millions of colors    
    '(\d*) x (\d*)', # 1366 x 768
    '(\d*)x(\d*)', # 1920x1200
    ]
RESOLUTION_REGEXS = regex_compile(RESOLUTION_REGEXS)

def parse_resolution(values):
    for value in values:
        s = clean(value).lower()
        for rc in RESOLUTION_REGEXS:
            match = c_regex_search(rc, s)
            if not match:
                continue
            count = len(match.groups())
            if not count:
                return [value, None, None]
            if not match.group(1):
                continue
            if count > 1:
                if not match.group(2):
                    continue
                width = int(match.group(1).replace('.', ''))
                height = int(match.group(2).replace('.', ''))
            else:
                width = None
                height = int(match.group(1).replace('.', ''))
            if width < 99 or height < 99:
                continue
            return [value, width, height]
                            

###########
# Crawler #
###########
class NotebookSpider(DesktopSpider):
    name = 'notebook'
    start_urls = ('http://www.lazada.co.id/beli-laptop/',)
    product_class = Notebook
    blacklist_brand_file = os.path.join('blacklist', 'brand', 'notebook.txt')
            
    def parse_resolution(self, response, i):        
        resolution = parse_resolution(i['description'])
        if resolution:
            i['resolution'] = resolution
        return i
                        
    # Override parent method
    def parse_before_plain_description(self, response, i):
        return self.parse_resolution(response, i)
